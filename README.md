# Derinio Sim

DerinioSim is an autonomous driving simulation built in Unity3D using C#.

The master branch contains only the required assets to run/configure the core simulation,i.e the overall car behavior (applying torques, steering, keeping distances from other cars etc) and car paths.

Before, proceeding to analyze the main components, let's take a quick look on the available scenes and their correspondent purposes:

### Scenes

1. **Configuration Scene**: In that scene, the user can configure/debug the desired paths for the automotive simulation
2. **Main Scene**: By using the saved configurations from the previous scene, a user can experience an auto/manual driving simulation.

Then, we proceed to describe the main assets of the project:

### Assets

1. **Nature**: Nature Objects such as trees, bushes etc. Mostly related to the environment and background. We grouped them to avoid confusion with custom packages that one may create. In general they should not be modified.
2. **Prefabs**: Prefabs that are actively used to the simulation such as NPC cars and points
3. **Windridge City**: Same principles as **Nature** folder.

Following, we will analyze the scripts, how car path configuration is established and how a car function cycle takes place. 

### Folders

1. **General**: Folder that provide general functionalities such as finding the nearest object with a specified layer, the minimum distance or demonstrate a pseudorandom behavior.
2. **Intro Scene**: Folder that contains everything related to the configuration of every possible car path in the scene.
3. **Main Scene**: Folder that contains every additional, in comparison with the intro scene, functionality related to the main scene.
4. **Car Behavior**: Folder that contains all the car behavior functionalities.
5. **Player Related Scripts**: Additional scripts related to handling specific functionalities mainly for the player, such as switching from auto to manual mode.  

All the main function handling is being done through the Data script. This script is attached on the CarPlayer gameobject and is the main script of the simulation. Inside you can see that a series of other helpful classes are called and updated on every frame. The main output of the Data script can be seen on the Unity Editor, by clicking the PlayerCar when running the simulation.

## Configuration Scene 

### Path Finding

#### Basic Assumptions 

We assume that a car is either turning or going straight.
Whenever we want the car to change its orientation, even the smallest modification, we consider it a turn
|Proper Turn |RoundAbout (3 turns)|Small Turn |
|:-------------------------:|:-------------------------:|:-------------------------:|
<img width="215" src="/Photos/TypesOfTurns/ProperTurn.jpg"> |<img width="215" src="/Photos/TypesOfTurns/RoundAbout.jpg">|<img width="215" src="/Photos/TypesOfTurns/SmallTurn.jpg">|

#### I want to create my own path. How should my point's coordinate system be oriented?

* For **Start Points** the **RED** axis should be pointing to the front Midpoint.
* For **Mid Points** the **RED** axis should,generally, point straight ahead. In case I want a short turn, I should, point the ***RED*** axis as straight as I can to the target StartPoint.

#### Which are the type of points?

We have three types of points:

1. **Spawn Point**: Cars are being spawned here.
2. **Start Point**: You'll find them at the start of each ***straight line*** and at the end of each ***turn***.
3. **Mid Point**: You'll find them at the start of each ***turn*** and at the end of each ***straight line***.

#### How straight lines are being generated?

No lines are generated, we just assign in the ***Start Point*** the front ***Mid point*** throught a script.  
As a result, whenever a car is near a ***Start Point***, it will be directed to the correspondent ***Mid Point***.

#### How turns are being generated?

Before analyzing the turns, we should consider the basic steps included in turn generation:

1. Find the target Start Point
2. Spawn three extra control points (*Point Closest To Mid Point*,*Intersection Point*,*Point Closest To Start Point*)
3. Interpolate using Catmull-Rom throught the set of these 5 points:
   a.Mid Point
   b.Point Closest To Mid Point
   c.Intersection Point
   d.Point Closest To Start Point
   e.Start Point
4. Spawn the spline

|Proper Turn |Short Turn |
|:-------------------------:|:-------------------------:|
<img width="215" src="/Photos/Splines/ProperTurn.JPG"> | <img width="215" src="/Photos/Splines/ShortTurn.JPG">|

#### How these extra control points are being spawned? 

Taking into account that we have two kinds of turns (a really short and the "proper") we use a different method for each one.


Short Turn:
1. Spawn the IntersectionPoint in the middle of the distance between MidPoint and StartPoint
2. Same for ClosestToMidPoint for the distance between MidPoint and IntersectionPoint
3. Sane for ClosestToStart for the distance between IntersectionPoint and StartPoint
```csharp
...
// if angle between the two vectors is smaller than 3 degrees
if (Vector3.Angle(onNormal, dirFromMidToStartPoint.normalized) <= 3f) //Short Turn
{
    positionOfIntersectionPoint = midPoint.transform.position + dirFromMidToStartPoint.normalized * dirFromMidToStartPoint.magnitude / 2f;
    positionClosestToMidPoint = midPoint.transform.position + dirFromMidToStartPoint.normalized * dirFromMidToStartPoint.magnitude / 4f;
    positionClosestToStart = midPoint.transform.position + dirFromMidToStartPoint.normalized * dirFromMidToStartPoint.magnitude * 3f / 4f;
}
```

Proper Turn:
1. Find the Vector from MidPoint to StartPoint
2. Create the projection of the previous vector on MidPoint.transform.right Vector
3. Spawn the IntersectionPoint in the *MidPoint.transform.right* direction by adding to MidPoint.transform.position the  magnitude of the projection vector + an offset.
4. Spawn the ClosestToMidPoint  *MidPoint.transform.right* direction by adding to MidPoint.transform.position the half magnitude of of the projection vector
5. Spawn the ClosestToStart in the  normalized *dirFromIntersectionPointToStart* direction by adding to positionOfIntersectionPoint the half magnitude of the dirFromIntersectionPointToStart vector. Last but not least, we add in the *MidPoint.transform.right* direction multiplied by an offset1.

```csharp
else //Proper Turn
{
    positionOfIntersectionPoint = midPoint.transform.position + onNormal * (projectionOfVectorAtoB.magnitude + offset);
    positionClosestToMidPoint = midPoint.transform.position + onNormal * (projectionOfVectorAtoB.magnitude / 2f);
    Vector3 dirFromIntersectionPointToStart = startPoint.transform.position - positionOfIntersectionPoint;
    positionClosestToStart = positionOfIntersectionPoint + dirFromIntersectionPointToStart.normalized * (dirFromIntersectionPointToStart.magnitude / 2f + offset)+midPoint.transform.right*offset1;
}
...
```

|Proper Turn |Short Turn |
|:-------------------------:|:-------------------------:|
<img width="215" src="/Photos/TurnAlgorithms/turn.jpg"> |<img width="215" src="/Photos/TurnAlgorithms/ShortTurn.jpg">|

#### Procedure of Path Finding/Creating 

|Path Finding |Short Turn |
|:-------------------------:|:-------------------------:|
<img width="215" src="/Photos/HowPathFindingWorks.jpg"> |<img width="215" src="/Photos/HowSpawnWorks.jpg">|

Path finding procedures, can be located on **Configuration Scene**. In hierarchy, an individual may find, the *GeneralManager* gameobject which has an instance of SimulationManagement.cs and two child gameobjects, namely *Point Manager* and *Car Spawner*.

###### SimulationManagement.cs

```csharp
_pointRegister.AssignPossiblePaths(); //Create all the possible paths that a car can take 
_carSpawner.SpawnTheCars(); //Spawn a number of cars to test your paths
if (_saveAssets) // In case you want to auto spawn prefabs of these new assets (new Spawnpoints and Paths) to use them in main scene.
{...}
```
###### AssignPossiblePaths (...)
* Assign the proper .cs to every point w.r.t their layer
```csharp
public GameObject _paths; //Get the root element of all points
public void AssignPossiblePaths()
{
    LayerMask midPointlayer = LayerMask.NameToLayer("Path_MidPoint");
    LayerMask startPointlayer = LayerMask.NameToLayer("Path_Start");

    foreach (Transform child in _paths.transform)
    {
        if (child.gameObject.layer == startPointlayer) child.gameObject.AddComponent<StartPoints>();
        else if (child.gameObject.layer == midPointlayer) child.gameObject.AddComponent<MidPoints>();
    }
...
```
* Get the front *MidPoint* for each *StartPoint* and save it to the correspondent instance of StartPoints.cs
* Inform that *MidPoint* that it shouldn't (when searching for StartPoints) take into account that *StartPoint*.
```csharp
...
    foreach (Transform child in _paths.transform)
    {
        if (child.gameObject.layer == startPointlayer)
        {
            child.gameObject.GetComponent<StartPoints>()._firstMidPoint = StartPoints.AssignTargetMidPoint(child.gameObject);
            child.gameObject.GetComponent<StartPoints>()._firstMidPoint.GetComponent<MidPoints>().StartPointDirectlyBehindMe = child.gameObject;
        }
    }
...
```
* Find every available StartPoint for each MidPoint
* Save the correspondent splines
```csharp
...
    foreach (Transform child in _paths.transform)
    {
        if (child.gameObject.layer == midPointlayer)
        {
            child.gameObject.GetComponent<MidPoints>()._targetStartPoints = MidPoints.StartPointsInRange(child.gameObject.transform, child.gameObject.GetComponent<MidPoints>().StartPointDirectlyBehindMe, child.gameObject.tag);
        }
    }
    foreach (Transform child in _paths.transform)
    {
        if (child.gameObject.layer == midPointlayer)
        {
            child.gameObject.GetComponent<MidPoints>().startTargets = MidPoints.GenerateTurns(child.gameObject, child.gameObject.GetComponent<MidPoints>()._targetStartPoints);
        }
    }
}
```


## How a Car navigates during the Simulation?
|Engine Methods |
|:-------------------------:|
<img width="1024" src="/Photos/EngineMethods.jpg"> |

|Question New Node |
|:-------------------------:|
<img width="1024" src="/Photos/Scripts/QuestionNewNode.jpg"> |

#### NOTE:
Master branch is currently under refactoring, thus documentation is constantly updated.
