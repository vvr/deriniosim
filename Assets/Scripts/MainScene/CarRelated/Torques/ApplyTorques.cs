﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class ApplyTorques
{
    private static readonly float maxTorque = 2000f;
    private static readonly float breakTorque = 5000f;
    #region Public Methods
    public static void Drive(ref float wheelFrontLeftmotorTorque, ref float wheelFrontRightmotorTorque, ref float wheelFrontLeftbreakTorque, ref float wheelFrontRightbreakTorque, bool GO,bool STOP,bool Auto,bool Reverse,bool justRoll)
    {
        if (GO)
        {
            wheelFrontLeftmotorTorque = Auto ? maxTorque : maxTorque/*mainScript.GasPedal*/;
            wheelFrontRightmotorTorque = Auto ? maxTorque : maxTorque /* mainScript.GasPedal*/;
            wheelFrontLeftbreakTorque = 0;
            wheelFrontRightbreakTorque = 0;
        }
        else if (STOP)
        {
            wheelFrontLeftmotorTorque = 0;
            wheelFrontRightmotorTorque = 0;
            wheelFrontLeftbreakTorque = breakTorque;
            wheelFrontRightbreakTorque = breakTorque;
        }
        else if(!Auto && Reverse)
        {
            wheelFrontLeftmotorTorque = Auto ? -maxTorque : -maxTorque /* mainScript.Break_Reverse_Pedal*/;
            wheelFrontRightmotorTorque = Auto ? -maxTorque : -maxTorque /* mainScript.Break_Reverse_Pedal*/;
            wheelFrontLeftbreakTorque = 0;
            wheelFrontRightbreakTorque = 0;
        }
        else if (!Auto && justRoll)
        {
            wheelFrontLeftmotorTorque = 0;
            wheelFrontRightmotorTorque = 0;
            wheelFrontLeftbreakTorque = 0;
            wheelFrontRightbreakTorque = 0;
        }
    }
    #endregion
}
