﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AgentSimulationEnum
{
    JustSpawned,
    MovingTowardsMidPoint,
    MovingTowardsStart
}
