﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateSteeringWheel : MonoBehaviour
{
    public Data Car;
    float timer = 0f;
    float prevSteer = 0f;
    void Update()
    {
        if (timer > 1f) timer = 0f;
        timer += Time.deltaTime;
        float appliedSteer = Mathf.LerpAngle(prevSteer, -Car.wheelFrontLeft.steerAngle, timer);
        transform.localRotation = Quaternion.Euler(15, 0, appliedSteer);
        prevSteer = appliedSteer;
    }
}
