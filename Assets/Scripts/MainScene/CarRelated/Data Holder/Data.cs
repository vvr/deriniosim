﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    #region Attributes
    public bool Auto = true;
    [Header("AutoDrive Info")]
    public bool GO = false;
    public bool STOP = false;
    public bool Reverse = false;
    public bool justRoll = false;
    public bool SlowDown = false;
    [Header("Reference")]
    public GameObject Reference;
    [Header ("Car Driving Status")]
    public AgentSimulationEnum agentStatus;
    [Header("Turning")]
    public bool Turning;
    public List<Vector3> splinePoints;
    public int currentIndex;
    [Header("Node Related")]
    public GameObject targetNode;
    [Header("Current Lane")]
    public string CurrentLane;
    [Header("Side Lane")]
    public string SideLane;
    [Header("Velocity, Boundary Speeds and Booleans",order =0)]
    [Header("Current Speed", order = 1)]
    public float currentSpeed;
    [Header("Speeds")]
    public float currentMaxTopSpeed;
    public float maxStraightSpeed;
    public float maxTurningSpeed;
    [Header("Exceeding Speed?")]
    public bool _exceededTopVelocity = false;
    [Header("Torques")]
    public float maxTorque;
    public float maxBreakTorque;
    [Header("Car Sensors")]
    public float sensorSideLength = 20f;
    [Header("Nearby Cars,Distances and Booleans", order = 0)]
    [Header("Front Side", order = 1)]
    public bool _carInFrontOfMe = false;
    public GameObject _frontSideCar;
    public float _distanceFrontSideCar;
    [Header("Right Side", order = 1)]
    public GameObject _RightSideCar;
    public float _distanceRightSideCar;
    [Header("Left Side", order = 1)]
    public GameObject _LeftSideCar;
    public float _distanceLeftSideCar;
    [Header("Side Car Is Turning?")]
    public bool _sideCarNeedsToturn;
    [Header("Nearby Peds,Distances and Booleans", order = 0)]
    [Header("Front Side", order = 1)]
    public GameObject frontSideObject;
    public float DistanceFrontObject;
    [Header("Right Side", order = 1)]
    public GameObject rightSideObject;
    public float DistanceRightObject;
    [Header("Left Side", order = 1)]
    public GameObject leftSideObject;
    public float DistanceLeftObject;
    [Header("Traffic Lights")]
    public bool _redLight;
    [Header("Stop Signs")]
    public bool _stopSigns;
    [Header("Gamified UI")]
    public int GasPedal = 0;
    public int Break_Reverse_Pedal = 0;
    [Header("Change Lane Related")]
    public bool canChangeLane = false;
    public bool wantToChangeLane = false;
    public bool ChangingLane = false;
    public GameObject ChangeLaneTargetNode;
    public GameObject ChangeLaneNextnode;
    [Header("Etc")]
    public Renderer bodyPaintMat;
    [Header("Wheel Colliders")]
    public WheelCollider wheelFrontLeft;
    public WheelCollider wheelFrontRight;
    public WheelCollider wheelRearLeft;
    public WheelCollider wheelRearRight;

    private Rigidbody rbody;
    private bool isPrevAuto = false;
    #endregion

    #region Engine Methods
    private void Awake()
    {
        rbody = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        if (Auto) HighLevelCarBehavior.InitializeMethodsForAuto(ref maxStraightSpeed, ref maxTurningSpeed, ref targetNode, gameObject, CurrentLane, agentStatus);

    }
    private void OnEnable()
    {
        try
        {
            WheelCollider WheelColliders = GetComponentInChildren<WheelCollider>();
            WheelColliders.ConfigureVehicleSubsteps(5, 12, 15);
        }
        catch { Debug.Log("Problem with WheelColliders"); };
    }
    private void FixedUpdate()
    {
        float wheelFrontLeftMotorToqrue=0f;
        float wheelFrontRightMotorToqrue=0f;
        float wheelFrontLeftMotorBreak =0f;
        float wheelFrontRightMotorBreak =0f;

        HighLevelCarBehavior.CarMechanicalSystem(ref wheelFrontLeftMotorToqrue, ref wheelFrontRightMotorToqrue, 
            ref wheelFrontLeftMotorBreak , ref wheelFrontRightMotorBreak , ref rbody, 
            wheelFrontLeft, wheelFrontRight, wheelRearLeft, wheelRearRight, 
            Reference, gameObject, targetNode, currentIndex, splinePoints, GO, STOP, Auto, Reverse, justRoll, Turning);

        wheelFrontLeft.motorTorque = wheelFrontLeftMotorToqrue;
        wheelFrontRight.motorTorque = wheelFrontRightMotorToqrue;
        wheelFrontLeft.brakeTorque = wheelFrontLeftMotorBreak ;
        wheelFrontRight.brakeTorque = wheelFrontRightMotorBreak ;
    }
    private void Update()
    {
        if (isPrevAuto && Turning) Auto = true ;
        if (Auto)
        {
            HighLevelCarBehavior.Scanners(ref _frontSideCar, ref _distanceFrontSideCar,
            ref _RightSideCar, ref _distanceRightSideCar,
            ref _LeftSideCar, ref _distanceLeftSideCar,
            ref frontSideObject, ref DistanceFrontObject,
            ref rightSideObject, ref DistanceRightObject,
            ref leftSideObject, ref DistanceLeftObject,
            ref _redLight,ref _stopSigns,gameObject,
            SideLane,Reference.transform,Turning);

            HighLevelCarBehavior.CarSoftwareSystem(ref SideLane, ref GO,
            ref STOP, ref Turning,
            ref currentIndex, ref SlowDown,
            ref targetNode, ref agentStatus,
            ref canChangeLane, gameObject,
            ref currentSpeed, ref _carInFrontOfMe,
            _frontSideCar, Reference,
            ref splinePoints, CurrentLane,
            ref _exceededTopVelocity,ref _sideCarNeedsToturn, 
            maxStraightSpeed, maxStraightSpeed, 
            maxTurningSpeed, _carInFrontOfMe,
            _LeftSideCar,_RightSideCar,
            _redLight,_stopSigns);
        }
        else
        {
            Manual.WASD(ref GO, ref STOP, ref justRoll, ref Reverse, ref Turning);
            CarUpdateHelper.UpdateVehicleSpeed(ref currentSpeed, gameObject);
        }
        isPrevAuto = Auto;
    }
    #endregion
}

