﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseSpeed
{
    public static int _regularMaxSpeed = 10;
    public static int _turningMaxSpeed = 7;
    public static int _offset = 1;
    public static void PickRandomSpeed(ref float StraightAheadSpeed,ref float TurningSpeed)
    {
        StraightAheadSpeed = UnityEngine.Random.Range(_regularMaxSpeed - _offset, _regularMaxSpeed + _offset);
        TurningSpeed= UnityEngine.Random.Range(_turningMaxSpeed - _offset, _turningMaxSpeed + _offset);
    }
}
