﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CheckDistancesFromCars
{
    #region Public Methods
    public static void KeepDistanceFromFrontCar(ref bool carInFrontSTOP, GameObject carInFrontOfMe)
    {
        CheckCarInFrontOfMe(ref carInFrontSTOP, carInFrontOfMe);
    }
    #endregion

    #region Private Methods
    private static void CheckCarInFrontOfMe(ref bool carInFrontOfMe, GameObject frontSideCar)
    {
        carInFrontOfMe = !(frontSideCar == null);
    }
    #endregion
}
